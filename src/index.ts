import { Telegraf } from "telegraf";

import { ListCommands, SplitCommand } from "./helpers/helper";
import { CheckListItem } from "./models/checklistitem";
import { Checklist } from "./database/checklist";
import { bold, bullet, capitalize, strikethrough } from "./helpers/formatter";

let TelegramBot: Telegraf

if (process.env.TELEGRAM_BOT_API) {
    TelegramBot = new Telegraf(process.env.TELEGRAM_BOT_API)
}
else {
    throw new Error("TELEGRAM_BOT_API not set.");
}

// Start
TelegramBot.command('start', (ctx) => {
    ctx.reply(
        `Tuturu~ I'm Nosla your personal asssistant!\n\n` +

        `Type /help to get started!`
    )
})

// Help
TelegramBot.command('help', (ctx) => {
    ctx.reply(
        'I can organize your checklist with your family and friends!\n' +
        'How can I help you?:\n\n' +
        ListCommands()
    )
})

// Add Item
TelegramBot.command('add', (ctx) => {

    var CommandArgs = SplitCommand(ctx.message.text)
    if (CommandArgs.args) {
        // const encrypted = Encrypt(CommandArgs.args)
        // ctx.reply(`${CommandArgs.args} => ${encrypted}`)

        const itemName = CommandArgs.args
        const item = new CheckListItem(itemName)
        Checklist.add(ctx.chat.id.toString(), item, (result) => {

            if (result) {
                ctx.replyWithHTML(`I have added ${bold(itemName)} to the list!`)
            }
            else {
                ctx.reply(`Sorry, I'm unable to add the item at this moment. Please try again later.`)
            }
        })
    }
    else {
        ctx.reply("Please state the item you would like to add with /add <item>.")
    }
})

// List Items
TelegramBot.command('list', (ctx) => {

    const chatId = ctx.chat.id.toString()
    Checklist.list(chatId, (items) => {

        var output = `Here are the items in your list:\n${'—'.repeat(14)}\n`
        items.forEach((item) => {

            var itemStr = `${bullet(capitalize(item.name))} [x${item.quantity}]`
            if (item.check) itemStr = strikethrough(itemStr)
            output += `${itemStr}\n`
        })

        ctx.replyWithHTML(output)
    })
})

// Delete Item
TelegramBot.command('remove' || 'delete' || 'rm' || 'del', (ctx) => {

    var CommandArgs = SplitCommand(ctx.message.text)

    if (CommandArgs) {

        const itemName = CommandArgs.args
        Checklist.remove(ctx.chat.id.toString(), itemName, (result) => {

            switch (result) {
                case "Success":
                    ctx.replyWithHTML(`I have deleted ${bold(itemName)} from the list.`)
                    break

                case "Failure":
                    ctx.reply(`Sorry I'm unable to remove the item at this moment. Please try again later.`)
                    break

                case "Not Found":
                    ctx.replyWithHTML(`${bold(itemName)} has not been added to the list.`)
                    break
            }
        })
    }
})

// Clear
TelegramBot.command('clearall', (ctx) => {

    Checklist.clear(ctx.chat.id.toString(), (result) => {
        
        if (result) {
            ctx.reply('I have cleared all the items in the list.')
        }
        else {
            ctx.reply(`Sorry I'm unable to remove the item at this moment. Please try again later.`)
        }
    })
})

// Launch Bot
TelegramBot.launch()
process.once('SIGINT', () => TelegramBot.stop('SIGINT'))
process.once('SIGTERM', () => TelegramBot.stop('SIGTERM'))
