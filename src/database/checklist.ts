import { MongoClient } from "mongodb";
import { CheckListItem } from "../models/checklistitem";
import * as assert from "assert"

export class Checklist {

    /**
     * Connect to MongoDB
     * @param callback Returns MongoClient
     */
    private static connect(callback: (client: MongoClient) => void) {
        assert(process.env.MONGO_URL, "MongoDB Connection String not set.")
        MongoClient.connect(process.env.MONGO_URL, { authSource: 'admin' }, (error, client) => {
            if (error) {
                throw error
            }
            else {
                console.log('Successfully connected to MongoDB')
            }

            callback(client)
        })
    }


    /**
     * Add item into list
     * @param chatId Location of Command
     * @param item Item to be added
     * @param callback Result of adding to database
     */
    static add(chatId: string, item: CheckListItem, callback: (result: Boolean) => void) {

        // Connect to MongoDB
        this.connect((client) => {

            // Get chatId Collection
            const db = client.db()
            const collection = db.collection(chatId)

            // Insert item
            collection.insertOne(item, (error, _) => {

                if (error) {
                    console.error(error.message, error.errmsg)
                    callback(false)
                }
                else {
                    console.log(`Adding item ${item.name} into ${chatId}`)
                    callback(true)
                }
            })

            // Close Connection
            client.close()
        })
    }

    /**
     * Get all items from checklist
     * @param chatId Location of Command
     * @param callback Array of checklist itemss
     */
    static list(chatId: string, callback: (items: Array<CheckListItem>) => void) {

        // Connect to MongoDB
        this.connect(async (client) => {

            // Get chatId Collection
            const db = client.db()
            const collection = db.collection(chatId)

            // Get Items
            const jsonItems = await collection.find({}).toArray()
            const items: Array<CheckListItem> = jsonItems.map((document) => {
                return new CheckListItem(document.name, document.check, document.quantity)
            })

            callback(items)
        })
    }

    /**
     * Remove item from checklist
     * @param chatId Location of Command
     * @param item Item to remove
     * @param callback Result of removing from database
     */
    static remove(chatId: string, item: string, callback: (result: "Success" | "Failure" | "Not Found") => void) {

        // Connect to MongoDB
        this.connect(async (client) => {

            // Query Parameters
            const query = { name: item }

            // Get chatId Collection
            const db = client.db()
            const collection = db.collection(chatId)

            // Find and delete item
            collection.findOneAndDelete(query, (error, result) => {

                // Error while removing item
                if (error) {
                    console.error(error.message)
                    callback("Failure")
                }
                else {

                    if (result.value) {
                        // Item Found and removed
                        callback("Success")
                    }
                    else {
                        // Item Not Found
                        callback("Not Found")
                    }
                }
            })
        })
    }

    /**
     * Clear all items in checklist
     * @param chatId Location of Command
     * @param callback Result of Clearing Collection
     */
    static clear(chatId: string, callback: (result: boolean) => void) {
        
        // Connect to MongoDB
        this.connect((client) => {

            // Get chatId Collection
            const db = client.db()
            const collection = db.collection(chatId)

            // Drop Collection
            collection.drop((error) => {

                if (error) {
                    callback(false)
                }
                else {
                    callback(true)
                }
            })
        })
    }

}
