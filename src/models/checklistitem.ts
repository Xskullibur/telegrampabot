export class CheckListItem {

    name: string
    check: boolean
    quantity: number

    /**
     * Checklist Item Object
     * @param name Item Name
     * @param check Item Status
     * @param quantity Number of Items Needed
     */
    constructor(name: string, check: boolean = false, quantity: number = 1) {
        this.name = name
        this.check = check
        this.quantity = quantity
    }
}
