/// Markdown Formatter Functions

export function bold(str: string): string {
    return `<b>${str}</b>`
}

export function underline(str: string): string {
    return `<u>${str}</u>`
}

export function italic(str: string): string {
    return `<i>${str}</i>`
}

export function strikethrough(str: string): string {
    return `<del>${str}</del>`
}

export function capitalize(str: string) {
    const firstCP = str.codePointAt(0)!;
    const index = firstCP > 0xFFFF ? 2 : 1;

    return String.fromCodePoint(firstCP).toUpperCase() + str.slice(index);
}

export function bullet(str: string) {
    return ` • ${str}`
}