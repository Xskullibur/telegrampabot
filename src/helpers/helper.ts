import { AES, enc } from "crypto-js";

// Command List
const COMMANDS: { [key: string]: string } = {
    '/add': 'Add item to list',
    '/remove': 'Remove item from list',
    '/list': 'List items added',
    '/clearall': 'Clear all items in the list'
}

export function ListCommands(): string {
    return Object.keys(COMMANDS)
        .map((key) => { return `${key} - ${COMMANDS[key]}` })
        .join('\n')
}

// Encryption and Decryption
export function Encrypt(msg: string): string {

    if (process.env.SECRET_KEY) {
        return AES.encrypt(msg, process.env.SECRET_KEY).toString()
    }
    else {
        throw new Error("SECRET_KEY not set.");
    }
}

export function Decrypt(encrypted: string): string {

    if (process.env.SECRET_KEY) {
        var decrypt = AES.decrypt(encrypted, process.env.SECRET_KEY!!)
        return decrypt.toString(enc.Utf8)
    }
    else {
        throw new Error("SECRET_KEY not set.");
    }
}

// Command Helpers
/**
 * Split user entered commands
 * @param command User entered command (eg. /add apple)
 * @returns Command split based on the command, bot and its arguments/parameters
 */
export function SplitCommand(command: string): { text: string, command: string, bot: string, args: string } {

    const regex = /^\/([^@\s]+)@?(?:(\S+)|)\s?([\s\S]+)?$/i
    const commandArgs = regex.exec(command)

    if (commandArgs) {
        return {
            text: commandArgs[0],
            command: commandArgs[1],
            bot: commandArgs[2],
            args: commandArgs[3]
        }
    }
    else {
        throw new Error("SplitCommand Regex Error");
    }

}
